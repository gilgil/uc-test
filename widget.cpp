#include "widget.h"
#include "ui_widget.h"

#include <QByteArray>
#include <QMessageBox>
#include <QHostAddress>
#include <QThread>
#include <QUdpSocket>

struct MyThread : QThread {
	bool active_{true};
	QString ip_;
	int port_;
	MyThread(QObject* parent, QString ip, int port) : QThread(parent), ip_(ip), port_(port) {}
	void run() override {
		QHostAddress host(ip_);
		quint16 port = port_;
		QUdpSocket socket;


		QByteArray data;
		for (int i = 0; i < 1000; i++) data += 'a';
		data += "\r\n";

		socket.writeDatagram("start\r\n", host, port);
		while (active_) {
			socket.writeDatagram(data, host, port);
			QThread::sleep(1);
		}
		socket.writeDatagram("stop\r\n", host, port);
	}
};

MyThread *myThread = nullptr;


Widget::Widget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::Widget)
{
	ui->setupUi(this);
}

Widget::~Widget()
{
	delete ui;
}


void Widget::on_pbStart_clicked()
{
	if (myThread != nullptr) {
		QMessageBox::warning(this, "Error", "myThread is not null");
		return;
	}

	myThread = new MyThread(this, ui->leIp->text(), ui->lePort->text().toInt());
	myThread->start();
}


void Widget::on_pbStop_clicked()
{
	if (myThread == nullptr) {
		QMessageBox::warning(this, "Error", "myThread is null");
		return;
	}

	myThread->active_ = false;
	myThread->quit();
	if (!myThread->wait(5000)) {
		QMessageBox::warning(this, "Error", "myThread wait return false");
	}

	delete myThread;
	myThread = nullptr;
}

